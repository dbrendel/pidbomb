#!/bin/bash

num_clones=$(podman exec pidbombed ./tst-pidbomb)

if [ "$num_clones" -ne 2046 ]; then
    echo "Unexpected number of clones: $num_clones"
    exit 1
fi

if podman exec -it pidbombed echo 1 &>/dev/null; then
    echo "Could create new process"
    exit 1
fi

# create two processes
if ! sh -c "echo 1 | cat &>/dev/null"; then
    echo "Failed to create new process"
    exit 1
else
    echo "Success!"
    exit 0
fi
