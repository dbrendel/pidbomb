/* Copyright Dennis Brendel, Red Hat
 * SPDX-License-Identifier: GPL-2.0-only */

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <sched.h>
#include <unistd.h>

#include <sys/mman.h>

#define STACK_SIZE (1024 * 1024)

static int idle_func (__attribute__((unused)) void *arg)
{
    sleep (1);
    return 0;
}

static int do_test (void)
{
    int    num_clones = 0;
    char   *stack = NULL;
    char   *stack_top = NULL;

    stack = mmap (NULL, STACK_SIZE, PROT_READ | PROT_WRITE,
                  MAP_PRIVATE | MAP_ANONYMOUS | MAP_STACK, -1, 0);
    if (stack == MAP_FAILED)
    {
        perror ("mmap");
        return EXIT_FAILURE;
    }

    stack_top = stack + STACK_SIZE;

    while (clone (idle_func, stack_top, CLONE_PARENT, NULL) != -1)
    {
        num_clones++;
    }

    printf ("%d\n", num_clones);

    return 0;
}

int main (void)
{
    return do_test ();
}

