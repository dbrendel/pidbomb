CFLAGS += -Wall -Wextra -Werror -g

CONTAINER := pidbombed
CONTAINER_IMAGE := centos:stream9
RPMS := make gcc

SRC = tst-pidbomb.c
EXE = $(SRC:.c=)

$(EXE) : container
	@podman exec $(CONTAINER) $(CC) $(CFLAGS) $(SRC) -o $@

.PHONY : container
container:
ifeq ($(shell podman container exists ${CONTAINER}; echo $$?),1)
	@podman run --rm -itd \
		--volume $$PWD:$$PWD:z --workdir $$PWD \
		--name $(CONTAINER) $(CONTAINER_IMAGE)
	@podman exec -it $(CONTAINER) \
		dnf -y --setopt=install_weak_deps=false install $(RPMS)
endif

.PHONY : test
test : $(EXE)
	@./test.sh

.PHONY : clean
clean:
	$(RM) $(EXE)
	podman kill $(CONTAINER)
